document.querySelector('#reset')
  .addEventListener('click',scrollCurrent,'false');

function scrollCurrent() {
  scrollZero();
  document.querySelector('main').scrollLeft = document.querySelector('th.yellow').getBoundingClientRect().left;
}

document.querySelector('#start')
  .addEventListener('click',scrollZero,'false');

function scrollZero() {
  document.querySelector('main').scrollLeft = 0;
}

document.querySelector('#end')
  .addEventListener('click',function() {
    scrollZero();
    document.querySelector('main').scrollLeft = document.querySelector('th:nth-last-child(1)').getBoundingClientRect().left;
  },'false');

window.onload = scrollCurrent;
